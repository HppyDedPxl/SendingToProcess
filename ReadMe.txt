CHANGELOG


## v.0.1 Initial ##

- Save a pointer
- Get a saved Pointer : long long


- HookToProcess
- Get Process ID
- Get Module Base Address
- Get Thread Information (Handle, TIB/TEB, TBI)
- Get ThreadStack Top
- Resolve Multilevelpointer (llBuildPointer)
- List Modules of Process


- ReadLong
- ReadInt
- ReadFloat

- WriteLong
- WriteInt
- WriteFloat


More:
void hwndSetInternalWindowHandleByProcessID(DWORD PID);
PVOID GetThreadStackTopAddressOfThreadNr(int threadNr);