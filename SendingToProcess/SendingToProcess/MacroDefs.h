// By Alexander Konad, 2015
// http://allions.net Independent Project Group Allions
// http://alexgit.allions.net
#pragma once
/* Defs for later message posting .. will be obsolete with the expansion of process helper*/
#define dcml_PressLeftMouse \
	PostMessage(windowHandle, WM_LBUTTONDOWN, 0x01, 1);\
	Sleep(30);\
	PostMessage(windowHandle, WM_LBUTTONUP, 0x01, 1);

#define dcml_Wait(x) Sleep(x); 

#define dcml_RepeatTimes(x) for(int i = 0; i < x; i++){

#define dcml_EndRepeat }

#define dcml_RepeatUntil do{

#define dcml_EndRepeatUntilForever }while(true);

#define dcml_EndRepeatUntilFalse(c) }while(c==true);

#define dcml_MinimizeTarget PostMessage(windowHandle, WM_SYSCOMMAND, SC_MINIMIZE, 0);

#define dcml_MaximizeTarget PostMessage(windowHandle,WM_SYSCOMMAND, SC_MAXIMIZE, 0 );

#define dcml_EndRepeatUntilNULL(x) }while(c!=NULL)
