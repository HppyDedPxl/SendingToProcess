// By Alexander Konad, 2015
// http://allions.net Independent Project Group Allions
// http://alexgit.allions.net
#include "stdafx.h"
#include "ProcessHelper.h"

CProcessHelper::CProcessHelper()
{
}
CProcessHelper::CProcessHelper(const char* processName)
{
	HookToProcess(processName);
}

CProcessHelper::~CProcessHelper()
{
}


/*
	Initializes the object and precalculates crucial values
*/
void CProcessHelper::HookToProcess(const char* processName)
{
	CurrentProcessID = dwGetProcessId(processName);
	hwndSetInternalWindowHandleByProcessID(CurrentProcessID);
	CurrentProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, TRUE, CurrentProcessID);
}

#pragma region Writing And Reading (For the novelty of readability)

bool CProcessHelper::ReadLong(LPCVOID address, long long* buffer)
{
	return ReadProcessMemory(CurrentProcessHandle, address, buffer, 8, 0);
}
bool CProcessHelper::ReadInt(LPCVOID address, int* buffer)
{
	return ReadProcessMemory(CurrentProcessHandle, address, buffer, 4, 0);
}
bool CProcessHelper::ReadFloat(LPCVOID address, float* buffer)
{
	return ReadProcessMemory(CurrentProcessHandle, address, buffer, 8, 0);
}

bool CProcessHelper::WriteLong(LPCVOID address, long long value)
{
	return WriteProcessMemory(CurrentProcessHandle,(LPVOID)address, &value, 8, 0);
}
bool CProcessHelper::WriteInt(LPCVOID address, int value)
{
	return WriteProcessMemory(CurrentProcessHandle, (LPVOID)address, &value, 4, 0);
}
bool CProcessHelper::WriteFloat(LPCVOID address, float value)
{
	return WriteProcessMemory(CurrentProcessHandle, (LPVOID)address, &value, 8, 0);
}

#pragma endregion

/*
	Get the Handle of a Window from the ProcessID
*/
void CProcessHelper::hwndSetInternalWindowHandleByProcessID(DWORD PID)
{
	/*
		Callback that filters if the processID that was passed to the EnumWindows function is the same pID behind the window
		using a static lambda with a capture list to set this-scope variables inside of the callback because adding a capturelist
		to the lambda below would change the method signature, thus making it not castable to a EnumWindows Callback.
	*/
	static auto cb = [this](HWND d){
		CurrentWindowHandle = d;
	};
	auto func = [](HWND hwnd, LPARAM lParam)->BOOL{
		// get the processID of the window we are looking at
		DWORD dwProcessId;
		GetWindowThreadProcessId(hwnd, &dwProcessId);
		// Compare ID's and set helper Variable if the processIDs match
		if (dwProcessId == lParam)
		{
			cb(hwnd);
			return FALSE;
		}
		return TRUE;

	};
	/* Execute search then return hWnd */
	EnumWindows(func, PID);

}

/* 
	Return Info about the the nth thread in the current process and writes it to the provided buffers 
	Thanks to Brandon from StackOverflow for pointing me in the right direction with the Method Pointer here. (Pardon)
*/
BOOL CProcessHelper::GetThreadInformation(int threadNr,HANDLE* handleBuffer,THREAD_BASIC_INFORMATION* tbiBuffer, NT_TIB* tibBuffer)
{
	
	THREADENTRY32 TE32 = { 0 };
	TE32.dwSize = (sizeof(TE32));
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, CurrentProcessID);
	int curThreadNr = 0;

	if (hSnapshot != INVALID_HANDLE_VALUE)
	{

		HMODULE hNtdll = LoadLibrary("ntdll.dll");

		// Set callingconventions for the method
		NTSTATUS(__stdcall*NtQueryInformationThread)(HANDLE hThread, THREADINFOCLASS threadInfoClass, PVOID threadInfo, ULONG threadInfoLength, PULONG returnLength);
		// override local method pointer so it points to our loaded process address
		NtQueryInformationThread = reinterpret_cast<decltype(NtQueryInformationThread)>(GetProcAddress(hNtdll, "NtQueryInformationThread"));


		// for each thread
		if (Thread32First(hSnapshot, &TE32))
		{
			do{
				// Filter so we only get threads of our process
				if (TE32.th32OwnerProcessID == CurrentProcessID)
				{
					// get thread handle
					HANDLE h = OpenThread(THREAD_ALL_ACCESS, FALSE, TE32.th32ThreadID);
					if (NtQueryInformationThread)
					{
						NT_TIB tib = { 0 };
						THREAD_BASIC_INFORMATION tbi = { 0 };
						// get tbi
						NTSTATUS status = NtQueryInformationThread(h, ThreadBasicInformation, &tbi, sizeof(tbi), nullptr);
						// if succeeded we found a properly working thread
						if (status >= 0)
						{
							// is it the thread we are looking for though?
							if (curThreadNr == threadNr)
							{
								ReadProcessMemory(CurrentProcessHandle, tbi.TebBaseAddress, &tib, sizeof(tib), nullptr);
								FreeLibrary(hNtdll);
								if (tbiBuffer != nullptr)
									(*tbiBuffer) = tbi;
								if (handleBuffer!=nullptr)
									(*handleBuffer) = h;
								if (tibBuffer != nullptr)
									(*tibBuffer) = tib;

								std::cout << "Found Thread Handle with TBI and TEB of Thread: " << std::dec << std::endl;
								return true;
								
							}
							else
							{
								// increment cur thread and search for the next active thread
								curThreadNr++;
							}


						}
					}	
				}
			} while (Thread32Next(hSnapshot, &TE32));
			
			
		}
		
	}
	return false;
	
}

/*
	Returns the Threads StackTop Address by calculating the offset backward from the threadstacks exit. 
	currently works only for 64bit architectures.
	Partially translated from this pascal code : https://code.google.com/p/cheat-engine/source/browse/trunk/Cheat%20Engine/CEFuncProc.pas#3717 by Dark_Byte of Cheat Engine 
*/
PVOID CProcessHelper::GetThreadStackTopAddress(THREAD_BASIC_INFORMATION tbi)
{

	long long stacktop = 0;
	
	if (MEMREAD_BYTES_CURRENT == MEMREAD_BYTES64){
		// read 8 bytes after TebBase
		ReadProcessMemory(CurrentProcessHandle, (LPCVOID)((long long)(tbi.TebBaseAddress) + 8), &stacktop, 8, 0);
	}
	else
	{
		Print("Finding StackTop for 32bit is not yet supported");
		return nullptr;
	}

	if (stacktop != 0)
	{
		// allocate heap memory to save the stack into
		LPVOID buffer = malloc(4096);
		
		// read the stack memory and save it to our buffer.
		if (ReadProcessMemory(CurrentProcessHandle, (LPCVOID)(stacktop - 4096), buffer, 4096, 0))
		{
			if (MEMREAD_BYTES_CURRENT == MEMREAD_BYTES64)
			{
				// Get the kernel32 base address
				long long k32base = dwGetModuleBaseAddress("kernel32.dll");

				// assume kernel32.dll is about 1m  large (according to memory scans, but more research has to be made).
				// todo: find a way to get kernel32 base and top dynamically
				// 
				long long k32hi = k32base + 1175551;

				// search for the threadexit, since the threadexit is the first address that points to an address inside of kernel32 we will look for that.
				// check each 8byte field and see if the address points to an address in kernel32.
				for (size_t i = (4096/8); i > 0; i--)
				{
					long long c = (long long)((long long**)buffer)[i];
					
					if (c>k32base && c<=k32hi)
					{
						free(buffer);
						std::cout << std::dec << "Found Thread-Stack-Top at" << std::hex << (stacktop - 4096 + i * 8) << std::dec << std::endl;
						return (PVOID)(stacktop - 4096 + i * 8);
					}	
				}
			}
			else{
				Print("Finding StackTop for 32bit is not yet supported");
				return nullptr;
			}
		}
	}

	return nullptr;
}

/*
	Returns the PID of a process from the process name
*/
DWORD CProcessHelper::dwGetProcessId(const char* processName)
{

	// Allocate some space for our processes
	PROCESSENTRY32 ProcessEntry32;
	ProcessEntry32.dwSize = (sizeof(ProcessEntry32));

	// create a snapshot of currently running processes
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	DWORD pID;

	if (hSnapshot != INVALID_HANDLE_VALUE)
	{

		// Take first process
		if (Process32First(hSnapshot, &ProcessEntry32))
		{
			do{
				// Compare Names
				if (strcmp(ProcessEntry32.szExeFile, processName) == 0)
				{
					pID = ProcessEntry32.th32ProcessID;
					Print("Found Process ID");
					break;
				}
			} while (Process32Next(hSnapshot, &ProcessEntry32)); // if there was no match continue with other processes
		}
		CloseHandle(hSnapshot);
	}
	else
	{
		Print(GetLastError())
	}


	return pID;

}


/*
	http://homeofgamehacking.de/showthread.php?tid=840
	Although comparisons with TChar often failed. Rewrote it with const char and used different casts and cmp functions
	to solve the issues.
	It's also important to note that the encoding needs to be set to multibyte
*/
DWORD CProcessHelper::dwGetModuleBaseAddress(const char* lpszModuleName)
{
	// Create snapshot of currently active modules in process
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, CurrentProcessID);
	DWORD dwModuleBaseAddress = 0;
	// Check if the handle was valid and a snapshot could be made
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		// Allocate some space for our module
		MODULEENTRY32 ModuleEntry32 = { 0 };
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);

		// start by taking the first module from the modules list....
		if (Module32First(hSnapshot, &ModuleEntry32))
		{
			do
			{
			
				// compare the module names, if they are equal we have found our module, return the Base Address				
				if (strcmp(ModuleEntry32.szModule, lpszModuleName) == 0)
				{
					dwModuleBaseAddress = (DWORD)ModuleEntry32.modBaseAddr;
					Print("Found Module Base Address");
					break;
				}
			} while (Module32Next(hSnapshot, &ModuleEntry32)); // else get the next module and repeat until there are no modules left
		}



		CloseHandle(hSnapshot);
	}
	else
	{
		Print(GetLastError())
	}
	return dwModuleBaseAddress;
}

/*
Builds and returns a multilevel pointer from a base pointer and an offset list. Optionally writes success status and errorcodes into buffers.
*/
long long CProcessHelper::llBuildPointer( long long basePointer, SIZE_T offsetsCount, long long* offsets, HANDLE* bufferProcessHandle, BOOL* successBuffer, DWORD* errorBuffer)
{
	//Retrieves the process handle and saves it to the buffer if specified
	
	if (bufferProcessHandle != nullptr)
	{
		(*bufferProcessHandle) = CurrentProcessHandle;
	}
	long long ptrNew = basePointer;

	//iterate over the provided offsets
	for (size_t i = 0; i < offsetsCount; i++)
	{
		// Unwrap the pointerlevels and add the offsets
		BOOL success = TRUE;
		if (i < offsetsCount - 1)
			success = ReadProcessMemory(CurrentProcessHandle, (LPCVOID)(ptrNew + offsets[i]), &ptrNew, MEMREAD_BYTES_CURRENT, 0);
		else // the last level is a simple add
			ptrNew += offsets[i];

		if (!success)
		{
			if (successBuffer != nullptr)
			{
				(*successBuffer) = success;

			}
			if (errorBuffer != nullptr)
			{
				(*errorBuffer) = GetLastError();
			}
			return NULL;
		}
	}
	return ptrNew;

}

void CProcessHelper::ListProcessModules()
{
	// Create snapshot of currently active modules in process
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, CurrentProcessID);
	DWORD dwModuleBaseAddress = 0;
	// Check if the handle was valid and a snapshot could be made
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		// Allocate some space for our module
		MODULEENTRY32 ModuleEntry32 = { 0 };
		ModuleEntry32.dwSize = sizeof(MODULEENTRY32);

		// start by taking the first module from the modules list....
		if (Module32First(hSnapshot, &ModuleEntry32))
		{
			do
			{
				// compare the module names, if they are equal we have found our module, return the Base Address
				Print(TEXT(ModuleEntry32.szModule));
				
			} while (Module32Next(hSnapshot, &ModuleEntry32)); // else get the next module and repeat until there are no modules left
		}



		CloseHandle(hSnapshot);
	}
	else
	{
		Print(GetLastError())
	}
}

PVOID CProcessHelper::GetThreadStackTopAddressOfThreadNr(int countNr){
	HANDLE h = 0;
	THREAD_BASIC_INFORMATION tbi = { 0 };
	NT_TIB tib = { 0 };

	GetThreadInformation(1, &h, &tbi, &tib);
	return GetThreadStackTopAddress(tbi);
}

long long CProcessHelper::GetSavedPtr(const char* c)
{
	// key exists
	if (SavedPtrMap.count(c))
	{
		return SavedPtrMap[c];
	}
	return 0;
}

void CProcessHelper::SavePtrAs(const char* c,long long p)
{

	SavedPtrMap[c] = p;
	
}