// By Alexander Konad, 2015
// http://allions.net Independent Project Group Allions
// http://alexgit.allions.net
#pragma once
#include <windows.h>
#include <psapi.h>
#include <iostream>
#include <map>

// snapshots 
#include <TlHelp32.h>
// for hex printing
#include <iomanip>

#include "MacroDefs.h"




#define Print(x) std::cout<<std::dec<<x<<std::endl;
#define PrintHex(x) std::cout << std::hex << x  << std::dec << std::endl;


#define hwndGetWindowHandle(szWindowName) FindWindow(NULL,szWindowName)
#define hGetProcessHandle(pID) OpenProcess(PROCESS_ALL_ACCESS,TRUE,pID);


/* 
	Declarations for structures needed to scan threads, 
    thanks to Brandon from Stack Overflow for helping out with finding the TEB!
	http://stackoverflow.com/questions/32297431/getting-the-tib-teb-of-a-thread-by-its-thread-handle-2015/32301394#32301394
*/

typedef LONG NTSTATUS;
typedef DWORD KPRIORITY;
typedef WORD UWORD;

typedef struct _CLIENT_ID
{
	PVOID UniqueProcess;
	PVOID UniqueThread;
} CLIENT_ID, *PCLIENT_ID;

typedef struct _THREAD_BASIC_INFORMATION
{
	NTSTATUS                ExitStatus;
	PVOID                   TebBaseAddress;
	CLIENT_ID               ClientId;
	KAFFINITY               AffinityMask;
	KPRIORITY               Priority;
	KPRIORITY               BasePriority;
} THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;

enum THREADINFOCLASS
{
	ThreadBasicInformation,
};

/* Declarations end*/

enum PROCHELPER_MEMREAD_BYTES{
	PROCHELPER_MEMREAD_BIT_32 = 4,
	PROCHELPER_MEMREAD_BIT_64 = 8
};

class CProcessHelper
{
public:
	CProcessHelper();
	CProcessHelper(const char*);
	~CProcessHelper();

	HWND CurrentWindowHandle = 0;
	DWORD CurrentProcessID = 0;
	HANDLE CurrentProcessHandle = 0;

	const DWORD MEMREAD_BYTES32 = 4;
	const DWORD MEMREAD_BYTES64 = 8;

	DWORD MEMREAD_BYTES_CURRENT = 0;  

	

	long long GetSavedPtr(const char*);
	void SavePtrAs(const char*, long long);

	void HookToProcess(const char* processName);
	void hwndSetInternalWindowHandleByProcessID(DWORD PID);
	DWORD dwGetProcessId(const char* processName);
	DWORD dwGetModuleBaseAddress(const char* lpszModuleName);
	BOOL GetThreadInformation(int, HANDLE*, THREAD_BASIC_INFORMATION*, NT_TIB*);
	PVOID GetThreadStackTopAddress(THREAD_BASIC_INFORMATION);
	long long llBuildPointer(long long basePointer, SIZE_T offsetsCount, long long* offsets, HANDLE* bufferProcessHandle, BOOL* successBuffer, DWORD* errorBuffer);
	void ListProcessModules();

	PVOID GetThreadStackTopAddressOfThreadNr(int countNr);

	bool ReadLong(LPCVOID address, long long*);
	bool ReadInt(LPCVOID address, int*);
	bool ReadFloat(LPCVOID address, float*);

	bool WriteLong(LPCVOID address, long long value);
	bool WriteInt(LPCVOID address, int value);
	bool WriteFloat(LPCVOID address, float value);


private:
	std::map<const char*, long long> SavedPtrMap = std::map<const char*, long long>();

	void SetWindowHandleCB();





};

