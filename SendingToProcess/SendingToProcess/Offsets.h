// By Alexander Konad, 2015
// http://allions.net Independent Project Group Allions
// http://alexgit.allions.net
#pragma once

/* For tests Minecraft offsets for hunger value from the threadstack */
int mcb_offset_hungerPtrLevel = 4;
long long mcb_offset_hungerOffsets[4] = { -0x00000F28,0xE8,0x2F8,0x10 }; // Starts from Threadstack (1)
