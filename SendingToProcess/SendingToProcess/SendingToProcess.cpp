// By Alexander Konad, 2015
// http://allions.net Independent Project Group Allions
// http://alexgit.allions.net


#include "stdafx.h"
#include <windows.h>
#include <psapi.h>
#include <iostream>
#include "Offsets.h"
#include "ProcessHelper.h"





int _tmain(int argc, _TCHAR* argv[])
{
	/* Set Up Process Helper */
	CProcessHelper* procHelper = new CProcessHelper();
	procHelper->MEMREAD_BYTES_CURRENT = PROCHELPER_MEMREAD_BIT_64;
	procHelper->HookToProcess("java.exe");

	/* get pointers */
	long long basePtr = (long long)procHelper->GetThreadStackTopAddressOfThreadNr(1);
	LPVOID hungerAddress = (LPVOID)procHelper->llBuildPointer(basePtr, 
														 mcb_offset_hungerPtrLevel, 
														 mcb_offset_hungerOffsets, 
														 nullptr, 
														 nullptr, 		
														 nullptr);
	/* play around */
	int val;
	procHelper->ReadInt(hungerAddress, &val);
	procHelper->WriteInt(hungerAddress, 10);
	procHelper->ReadInt(hungerAddress, &val);
	Print(val);


	system("pause");


	return 0;
}

